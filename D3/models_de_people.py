# -*- coding: utf-8 -*-

# This file is part of the MiniFlOp project.
# Copyright (c) 2019
# Authors: Iulian Ober, Paul Renaud-Goud, Pablo Seban, et al.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with this program. If not, see
# <http://www.gnu.org/licenses/>.
from django.db import models
from django.contrib.auth.models import User


# Attributs de User de django : username, first_name, last_name, email, is_staff, is_superuser
class Tutor(User):
    FULL_STAFF = 'fs'
    SUPP_STAFF = 'ss'
    TUTOR_CHOICES = ((FULL_STAFF, "Full staff"),
                     (SUPP_STAFF, "Supply staff"),)

    status = models.CharField(max_length=2,
                              choices=TUTOR_CHOICES,
                              verbose_name="Status",
                              default=FULL_STAFF)


class FullStaff(Tutor):

    class Meta:
        verbose_name = 'FullStaff'


class SupplyStaff(Tutor):
    employer = models.CharField(max_length=50,
                                verbose_name="Employeur ?",
                                default=None, null=True, blank=True)
    position = models.CharField(max_length=50,
                                default=None, null=True, blank=True)
    field = models.CharField(max_length=50,
                             verbose_name="Domaine ?",
                             default=None, null=True, blank=True)

    class Meta:
        verbose_name = 'SupplyStaff'


class Student(User):
    belong_to = models.ManyToManyField('base.Group', blank=True)

    def __str__(self):
        return str(self.username) + '(G:' + str(self.belong_to) + ')'
